var htmlSize = document.documentElement.clientWidth||document.body.clientWidth;
var dom = document.getElementsByTagName("html")[0];
dom.style.fontSize = htmlSize/85.4+"px";
window.addEventListener("resize",
	function(e){
		var htmlSize = document.documentElement.clientWidth||document.body.clientWidth;
		dom.style.fontSize = htmlSize/85.4+"px";
	}
);