import Vue from 'vue'
import Router from 'vue-router'
import mi from '@/view/mi'
import product from '@/view/product'
import login from '@/view/login'
import register from '@/view/register'
import cart from '@/view/cart'
import index from '@/view/index';

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'mi',
      component: mi
    },
    {
      path: '/product',
      name: 'product',
      component: product
    },
    {
      path:"/login",
      name:"login",
      component:login
    },
    {
      path:"/register",
      name:"register",
      component:register
    },
    {
      path:"/cart",
      name:"cart",
      component:cart
    }
  ]
})
