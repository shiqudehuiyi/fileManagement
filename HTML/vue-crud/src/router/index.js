import Vue from 'vue'
import Router from 'vue-router'
import HelloWorld from '@/components/HelloWorld'
import index from '@/view/index';
import indexNew from '@/view/indexNew';
import elementUI from '@/view/elementUI';
import home from '@/view/home';

Vue.use(Router)

export default new Router({
  mode:'history',
  routes: [
    {
      path: '/',
      name: 'index',
      component: index,
    },
    {
      path: '/index',
      name: 'indexNew',
      component: indexNew
    },
    {
      path: '/elementUI',
      name: 'elementUI',
      component: elementUI
    },
    {
      path: '/home',
      name: 'home',
      component: home
    }
  ]
})
