let htmlSize = document.documentElement.clientWidth || document.body.clientWidth;
  	let dom = document.getElementsByTagName("html")[0];
  	dom.style.fontSize = htmlSize / 120 + "px";
  	window.addEventListener("resize", (e) => {
  		let htmlSize = document.documentElement.clientWidth || document.body.clientWidth;
  		dom.style.fontSize = htmlSize / 120 + "px";
  	});
// 1920*1080  1440*960  1024*760